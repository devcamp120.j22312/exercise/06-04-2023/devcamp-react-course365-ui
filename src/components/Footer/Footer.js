import { Container, Grid } from "@mui/material";

const Footer = () => {
    return (
        <Container style={{ marginTop: "30px", borderTop: "1px solid #D8D8D8", height: "70px", backgroundColor: "#F7F7F7" }} maxWidth>
            <Grid container>
                <Grid item xs={4} style={{ marginLeft: "100px", padding: "25px", fontWeight: "600" }}>
                    © 2021 Ionic Course365. All Rights Reserved.
                </Grid>
                <Grid item xs={6}>
                    <Grid container spacing={2} style={{marginLeft: "400px", padding: "25px"}}>
                        <Grid item>
                            <a style={{color:"blue"}}>Privacy</a>
                        </Grid>
                        <Grid item>
                            <a style={{color:"blue"}}>Terms</a>
                        </Grid>
                        <Grid item>
                            <a style={{color:"blue"}}>Feedback</a>
                        </Grid>
                        <Grid item>
                            <a style={{color:"blue"}}>Support</a>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    )
}

export default Footer;