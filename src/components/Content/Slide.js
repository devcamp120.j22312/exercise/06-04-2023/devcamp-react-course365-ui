import * as React from 'react';

import Card from '@mui/joy/Card';

import avatar from "../../assets/images/ionic-207965.png"
import Grid from '@mui/joy/Grid';
import { Button } from '@mui/material';


export default function Slide() {
    return (
        <Card style={{ marginTop: "10px", border: "none", padding: "5px", boxShadow: "none" }}>
            {/* <CardOverflow> */}
                <Grid container style={{ height: "500px", backgroundColor: "#03BFFF" }}>
                    <Grid item xs={6} style={{ height: "300px", width: "400px", margin: "100px auto" }}>
                        <h3 className="text-white" style={{ fontWeight: "700" }}>Welcome to Ionic Course365 Learning Center</h3>
                        <p className="" style={{ fontWeight: "600", fontSize: "18px" }}>Ionic Course365 is an online learning and teaching marketplace with over 5000 courses and 1 million students. Instructor and expertly crafted courses, designed for the modern students and entrepreneur.
                        </p>
                        <Grid container>
                            <Grid item xs={6}>
                                <Button variant="contained" style={{ backgroundColor: "#FFC008", color: "black", fontSize: "14px" }}>Browse Courses</Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button variant="contained" style={{ backgroundColor: "white", color: "black", fontSize: "14px" }}>Become Trainer</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={6}>
                        <img src={avatar} alt="" style={{ height: "500px", marginLeft: "75px" }} />
                    </Grid>
                </Grid>

            {/* </CardOverflow> */}
            {/* <Divider /> */}
            {/* <CardOverflow
                variant="soft"
                sx={{
                    display: 'flex',
                    gap: 1.5,
                    py: 1.5,
                    px: 'var(--Card-padding)',
                    bgcolor: 'background.level1',
                }}
            >
            </CardOverflow> */}
        </Card>
    );
}