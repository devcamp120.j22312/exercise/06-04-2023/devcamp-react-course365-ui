import { Card, CardContent, CardMedia, Container, Grid, Typography } from "@mui/material";

import javascript from '../../assets/images/courses/course-javascript.jpg'
import graphql from '../../assets/images/courses/course-graphql.jpg'
import css3 from '../../assets/images/courses/course-css.jpg'
import wordpress from '../../assets/images/courses/course-wordpress.jpg'

import tedHawkins from '../../assets/images/teacher/ted_hawkins.jpg'
import juanitaBell from '../../assets/images/teacher/juanita_bell.jpg'
import clevaioSimon from '../../assets/images/teacher/clevaio_simon.jpg'

const PopularCourses = () => {
    return (
        <Container>
            <h3 style={{ marginTop: "30px", marginLeft: "20px" }}>Most popular</h3>
            <Grid container spacing={2}>
                <Grid item xs={3} style={{ marginTop: "20px" }}>
                    <Card>
                        <CardMedia>
                            <img src={css3} alt="" style={{ width: "100%" }} />
                        </CardMedia>
                        <CardContent>
                            <Typography style={{height: "50px"}}>
                                <a href="#" style={{ fontWeight: "600", textDecoration: "none" }}>
                                    CSS: ultimate CSS course from beginner to advanced</a>
                            </Typography>
                            <Typography style={{ marginTop: "5px" }}>
                                3h 56m Beginner
                            </Typography>
                            <Typography style={{ fontWeight: "600", marginTop: "5px" }}>
                                $600
                            </Typography>
                            <Typography style={{ textDecoration: "line-through", marginTop: "5px" }}>
                                $750
                            </Typography>
                        </CardContent>
                        <hr />
                        <Grid container>
                            <Grid item xs={3} className="text-center">
                                <img src={juanitaBell} alt="" style={{ width: "30px", height: "30px", borderRadius: "50%", marginBottom: "5px" }} />
                            </Grid>
                            <Grid item xs={6}>
                                <p>Juanita Bell</p>
                            </Grid>
                            <Grid item xs={3}>

                            </Grid>
                        </Grid>
                    </Card>

                </Grid>
                <Grid item xs={3} style={{ marginTop: "20px" }}>
                    <Card>
                        <CardMedia>
                            <img src={javascript} alt="" style={{ width: "100%" }} />
                        </CardMedia>
                        <CardContent>
                            <Typography style={{height: "50px"}}>
                                <a href="#" style={{ fontWeight: "600", textDecoration: "none" }}>
                                    Getting Started with Javascript</a>
                            </Typography>
                            <Typography style={{ marginTop: "5px" }}>
                                3h 34m Beginner
                            </Typography>
                            <Typography style={{ fontWeight: "600", marginTop: "5px" }}>
                                $300
                            </Typography>
                            <Typography style={{ textDecoration: "line-through", marginTop: "5px" }}>
                                $550
                            </Typography>
                        </CardContent>
                        <hr />
                        <Grid container>
                            <Grid item xs={3} className="text-center">
                                <img src={tedHawkins} alt="" style={{ width: "30px", height: "30px", borderRadius: "50%", marginBottom: "5px" }} />
                            </Grid>
                            <Grid item xs={6}>
                                <p>Ted Hawkins</p>
                            </Grid>
                            <Grid item xs={3}>

                            </Grid>
                        </Grid>
                    </Card>
                </Grid>
                <Grid item xs={3} style={{ marginTop: "20px" }}>
                    <Card>
                        <CardMedia>
                            <img src={wordpress} alt="" style={{ width: "100%" }} />
                        </CardMedia>
                        <CardContent>
                            <Typography style={{height: "50px"}}>
                                <a href="#" style={{ fontWeight: "600", textDecoration: "none" }}>
                                    Complete Wordpress themes & plugins</a>
                            </Typography>
                            <Typography style={{ marginTop: "5px" }}>
                                4h 30m Advanced
                            </Typography>
                            <Typography style={{ fontWeight: "600", marginTop: "5px" }}>
                                $900
                            </Typography>
                            <Typography style={{ textDecoration: "line-through", marginTop: "5px" }}>
                                $1050
                            </Typography>
                        </CardContent>
                        <hr />
                        <Grid container>
                            <Grid item xs={3} className="text-center">
                                <img src={clevaioSimon} alt="" style={{ width: "30px", height: "30px", borderRadius: "50%", marginBottom: "5px" }} />
                            </Grid>
                            <Grid item xs={6}>
                                <p>Clevaio Simon</p>
                            </Grid>
                            <Grid item xs={3}>

                            </Grid>
                        </Grid>
                    </Card>
                </Grid>
                <Grid item xs={3} style={{ marginTop: "20px" }}>
                    <Card>
                        <CardMedia>
                            <img src={graphql} alt="" style={{ width: "100%" }} />
                        </CardMedia>
                        <CardContent>
                            <Typography style={{height: "50px"}}>
                                <a href="#" style={{ fontWeight: "600", textDecoration: "none" }}>
                                    GraphQL: introduction graphQL for beginer</a>
                            </Typography>
                            <Typography style={{ marginTop: "5px" }}>
                                2h 15m Intermediate
                            </Typography>
                            <Typography style={{ fontWeight: "600", marginTop: "5px" }}>
                                $650
                            </Typography>
                            <Typography style={{ textDecoration: "line-through", marginTop: "5px" }}>
                                $850
                            </Typography>
                        </CardContent>
                        <hr />
                        <Grid container>
                            <Grid item xs={3} className="text-center">
                                <img src={tedHawkins} alt="" style={{ width: "30px", height: "30px", borderRadius: "50%", marginBottom: "5px" }} />
                            </Grid>
                            <Grid item xs={6}>
                                <p>Ted Hawkins</p>
                            </Grid>
                            <Grid item xs={3}>

                            </Grid>
                        </Grid>
                    </Card>
                </Grid>
            </Grid>
        </Container>
    )
}

export default PopularCourses;