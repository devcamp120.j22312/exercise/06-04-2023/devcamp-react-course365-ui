import PopularCourses from "./PopularCourses";
import RecommendedCourses from "./RecommendedCourses";
import Slide from "./Slide"
import TrendingCourses from "./TrendingCourses";

const Content = () => {
    return (
        <>
        <Slide />
        <RecommendedCourses />
        <PopularCourses />
        <TrendingCourses />
        </>
    )
}

export default Content;